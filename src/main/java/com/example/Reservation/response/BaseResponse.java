package com.example.Reservation.response;

import lombok.Data;

@Data
public class BaseResponse {
    private ErrorSchema errorSchema;
}
