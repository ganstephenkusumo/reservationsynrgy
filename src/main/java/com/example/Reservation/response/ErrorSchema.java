package com.example.Reservation.response;

import com.example.Reservation.entity.constant.Constant;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(value= JsonInclude.Include.NON_NULL)
public class ErrorSchema {
    private String errorCode;

//    public void setResponse(TempBaseResponse tempBaseResponse){
//        this.errorCode=tempBaseResponse.getErrorCode();
//    }

    public void setSuccessResponse(){
        this.errorCode= Constant.ErrorCode.SUCCESS;
    }
}
