package com.example.Reservation.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;

@Getter
@Setter
@ToString
public class TempBaseResponse {
    protected  String errorCode;
    protected Long epoch;

    public void setDefaultTimestamp(){
        Instant instant = Instant.now();
        this.epoch = instant.getEpochSecond();
    }
}
