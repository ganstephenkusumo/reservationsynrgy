package com.example.Reservation.entity.request.user;

import lombok.Data;

import java.math.BigInteger;

@Data
public class PostRegisterUser {
    private String name;
    private String password;
    private String email;
    private BigInteger telephone;
    private String role;
}
