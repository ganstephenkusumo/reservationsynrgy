package com.example.Reservation.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class CommonException extends RuntimeException{
    private static final long serialVersionUID = 124891;

    protected String errorCode;

    public CommonException (String errorCode) { this.errorCode = errorCode;}

}
