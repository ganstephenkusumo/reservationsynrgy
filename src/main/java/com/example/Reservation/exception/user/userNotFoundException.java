package com.example.Reservation.exception.user;

public class userNotFoundException extends RuntimeException{
    userNotFoundException(Long id) {
        super("Could not find employee " + id);
    }
}
