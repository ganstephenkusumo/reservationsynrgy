package com.example.Reservation.service;

import com.example.Reservation.entity.request.user.PostRegisterUser;
import com.example.Reservation.exception.CommonException;
import com.example.Reservation.model.User;
import com.example.Reservation.repository.UserRepository;
import com.example.Reservation.response.BaseResponse;
import com.example.Reservation.response.ErrorSchema;
import com.example.Reservation.util.MessageUtils;
import lombok.var;
import org.aspectj.bridge.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    MessageUtils messageUtil;

    public List<User> findAll(){
        var users = (List<User>) userRepository.findAll();
        return users;
    }

    public BaseResponse userRegister(PostRegisterUser requestBody){
        BaseResponse response = new BaseResponse();
        ErrorSchema errorSchema = new ErrorSchema();
        System.out.println(requestBody);
        try{
            userRepository.save(User.builder()
                    .name(requestBody.getName())
                    .password(requestBody.getPassword())
                    .email(requestBody.getEmail())
                    .telephone(requestBody.getTelephone())
                    .role(requestBody.getRole())
                    .build()
            );
            errorSchema.setSuccessResponse();
            response.setErrorSchema(errorSchema);
        }catch(CommonException e){
            throw e;
        }
        return response;
    }
}
