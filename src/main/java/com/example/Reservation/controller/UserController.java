package com.example.Reservation.controller;

import com.example.Reservation.entity.request.user.PostRegisterUser;
import com.example.Reservation.model.User;
import com.example.Reservation.response.BaseResponse;
import com.example.Reservation.service.UserService;
import lombok.var;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestController
@RequestMapping("/api")
public class UserController {
    private UserService userService;

    @GetMapping("/showUsers")
    public String findUsers(User user){
        System.out.println("showUsers");
        var users = (List<User>) userService.findAll();
        return "showUsers";
    }

    @PostMapping("/register")
    public BaseResponse registerUser(@RequestBody PostRegisterUser request){
        System.out.println("masuk");
        System.out.println(request);
        return userService.userRegister(request);
    }
}

